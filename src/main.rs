use getopts::Options;
use lazy_static::lazy_static;
use log::warn;
use regex::Regex;
use std::env;
use std::io;
use std::io::ErrorKind;
use std::process::Command;

lazy_static! {
    static ref RE: Regex = Regex::new(r"\x1b\[((?:\d+;)*(?:\d+))m").unwrap();
}

fn eightbit_to_rgb(x: u8) -> (u8, u8, u8) {
    let x = x - 16;
    let b = x % 6;
    let g = (x / 6) % 6;
    let r = (x / 36) % 6;
    (r * 51, g * 51, b * 51)
}

fn replace_ansi_escapes(input: &str) -> String {
    let mut s = String::with_capacity(input.len());
    let mut cur_offset = 0;

    for cap in RE.captures_iter(input) {
        let full = cap.get(0).unwrap(); //must exist
        s.push_str(&input[cur_offset..full.start()]);
        cur_offset = full.end();

        let args: Vec<u8> = cap
            .get(1)
            .map(|c| c.as_str())
            .iter()
            .flat_map(|s| s.split(';'))
            .filter_map(|s| s.parse().ok())
            .collect();

        let code = args[0];

        match code {
            39 => s.push_str("\x0399"),
            38 => {
                let colorspace: u8 = args[1];
                let (r, g, b) = match colorspace {
                    2 => (args[2], args[3], args[4]),
                    5 => eightbit_to_rgb(args[2]),
                    _ => continue
                };
                s.push_str(&format!("\x04{:02X}{:02X}{:02X}", r, g, b));
            }
            _ => (),
        }
    }
    s.push_str(&input[cur_offset..]);
    s
}

fn spawn_command(cmd: &str, args: &[String]) -> io::Result<String> {
    Command::new(cmd)
        .args(args)
        .output()
        .and_then(|o| String::from_utf8(o.stdout).map_err(|e| io::Error::new(ErrorKind::Other, e)))

    // println!("Unknown Command: {}", &args[1..].join(" "))
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut opts = Options::new();
    opts.optflag("", "irc", "replace ansi color with irc");
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(e) => {
            println!("Cannot parse: {}", &args[1..].join(" "));
            println!("{}", e);
            return;
        }
    };

    let mut output = match spawn_command(&matches.free[0], &matches.free[1..]) {
        Ok(output) => output,
        Err(e) => {
            println!("Cannot run: {}", &matches.free[0..].join(" "));
            println!("{}", e);
            return;
        }
    };

    if matches.opt_present("irc") {
        output = replace_ansi_escapes(&output);
    }

    println!("{}", output);
}
